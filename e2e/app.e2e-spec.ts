import { JenkinsTetPage } from './app.po';

describe('jenkins-tet App', () => {
  let page: JenkinsTetPage;

  beforeEach(() => {
    page = new JenkinsTetPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
